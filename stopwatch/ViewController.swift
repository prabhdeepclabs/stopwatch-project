//
//  ViewController.swift
//  stopwatch
//
//  Created by Bhasker on 1/16/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var timer = NSTimer() // initialises timer
    
    
    var milliSec = 0 // milli seconds
    
    var seconds = 0 // seconds
    
    var minutes = 0 // minutes
    
    
    var startPressedAgain = false // keeping track if start button pressed again
    
    
    // label diplaying time elapsed
    @IBOutlet weak var display: UILabel!
    
    
    // display previous lap time
    @IBOutlet weak var showLapTime: UILabel!
    
    var lapButtonEnable = false
    
    
    
    // start button pressed
    
    @IBAction func startTimer(sender: AnyObject) {
        
        if ( !startPressedAgain ) {
            
            timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: "updateTime", userInfo: nil, repeats: true)
            
            // timer started with time interval 0.01s and updateTime func called each time
            
            startPressedAgain = true
        }
        
        lapButtonEnable = true
    
    }
    
    
    // update minutes, seconds and milliseconds based on timer and display output
    func updateTime () {
        
        milliSec++
        
        if (milliSec == 100) {
            
            seconds++
            milliSec = 0
        }
        
        if ( seconds == 60) {
            
            minutes++
            seconds = 0
        }
        
        
        // convert minutes seconds and millisecond to string so that a leading 0 can be added to them
        
        var strMilliSec = milliSec < 10 ? "0" + String(milliSec) : String(milliSec)
        
        var strSeconds = seconds < 10 ? "0" + String(seconds) : String(seconds)
        
        var strMinutes = minutes < 10 ? "0" + String(minutes) : String(minutes)
        
        
        // display time
        
        display.text = "\(strMinutes) : \(strSeconds) : \(strMilliSec)"
        
    }
    
    
    // stop button pressed
    
    @IBAction func stopTimer(sender: AnyObject) {
        
        timer.invalidate() // stop timer
        
        startPressedAgain = false
    }
    
    
    // reset button pressed so resetting all variables and labels and stopping timer
    
    @IBAction func reset(sender: AnyObject) {
        
        timer.invalidate()
        
        startPressedAgain = false
        
        milliSec = 0
        seconds = 0
        minutes = 0
        
        display.text = "00 : 00 : 00"
        
        showLapTime.text = ""
        
        lapButtonEnable = false
        
    
    }
    
    
    @IBAction func lapTime(sender: AnyObject) {
        
        if ( lapButtonEnable ) {
            
            showLapTime.text = "lap time - " + display.text!
        
            milliSec = 0
            seconds = 0
            minutes = 0
        }
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

